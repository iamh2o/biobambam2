#!/bin/bash
D=`date "+%Y_%m_%d"`
P=`head -n 1 configure.ac | sed "s|AC_INIT(||;s|,.*||"`
set -x
git bundle create ../${P}_${D}.bundle --branches --tags
